package com.dev.commons.core.fragment_accessor.app_fragment;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.dev.commons.general.event.OnBackPressedListener;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

/**
 * Helper subclass to provide commonly used functionality out of the box.
 */
abstract public class RawBaseFragment extends Fragment {

    private String mFragmentTag;
    private OnBackPressedListener mOnBackPressedListener;

    public RawBaseFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    protected void showActionBar() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }

    protected void hideActionBar() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    protected void showBackButton() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected void hideBackButton() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    protected void setActionBarTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    protected void clearActionBarLogo() {

        Drawable clearDrawable = new ColorDrawable(getResources().getColor(android.R.color.transparent));
        ((AppCompatActivity) getActivity()).getSupportActionBar().setLogo(clearDrawable);
    }

    protected void setActionBarLogo(int drawableResId) {

        Drawable drawable = ContextCompat.getDrawable(
                getActivity(),
                drawableResId);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayUseLogoEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setLogo(drawable);
    }

    public String getFragmentTag() {

        if (mFragmentTag == null) {
            mFragmentTag = this.getClass().getSimpleName();
        }

        return mFragmentTag;
    }

    public void setFragmentTag(String fragmentTag) {
        mFragmentTag = fragmentTag;
    }

    protected void setBackButtonForFragmentStackEntryCount() {

        int backStackEntryCount = getActivity().getSupportFragmentManager().getBackStackEntryCount();

        ((AppCompatActivity) getActivity())
                .getSupportActionBar()
                .setDisplayHomeAsUpEnabled(backStackEntryCount > 1);
    }

    public OnBackPressedListener getOnBackPressedListener() {
        return mOnBackPressedListener;
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        mOnBackPressedListener = onBackPressedListener;
    }
}
