package com.dev.commons.core.fragment_accessor;


import com.dev.commons.core.fragment_accessor.app_fragment.RawBaseFragment;
import com.dev.commons.general.util.Logr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;


public class RawFragmentAccessor {

    /**
     * This is the critical instance property that ties to the fragmentContainer layout and corresponding fragment ID
     */
    private int mContainerViewId;

    private static RawFragmentAccessor sFragmentAccessor;

    private FragmentActivity mFragmentActivity;

    /**
     * Needs to be called be each Activity
     *
     * @param fragmentActivity
     * @param containerViewId
     */
    public static void setFragmentContainer(FragmentActivity fragmentActivity, int containerViewId) {

        sFragmentAccessor = new RawFragmentAccessor(fragmentActivity, containerViewId);
    }

    /*
    FIXME: On app exit sFragmentAccessor is null, but doesnt matter
     */
    public static RawFragmentAccessor getInstance() {

        if (sFragmentAccessor == null) {
            // warning only:
            RuntimeException runtimeException =
                    new RuntimeException("FragmentContainer not specified");

            String msg = "Throwing RuntimeException: " + runtimeException.getMessage();
            Logr.debug(msg);
            // Toaster.unexpectedError(sFragmentAccessor.mFragmentActivity, msg);

            throw new RuntimeException(msg);
        }

        return sFragmentAccessor;
    }

    public static void replaceWith(RawBaseFragment fragment) {

        sFragmentAccessor.replaceFragment(fragment);
    }


    public RawFragmentAccessor() { }

    private RawFragmentAccessor(FragmentActivity fragmentActivity, int containerViewId) {
        mFragmentActivity = fragmentActivity;
        mContainerViewId = containerViewId;
    }

    public String getActiveFragmentTag() {

        int backStackEntryCount = mFragmentActivity.getSupportFragmentManager().getBackStackEntryCount();

        if (backStackEntryCount == 0) {
            return null;
        }

        String tag = mFragmentActivity.getSupportFragmentManager().getBackStackEntryAt(mFragmentActivity.getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return tag;
    }

    public Fragment getActiveFragment() {

        String tag = getActiveFragmentTag();
        return mFragmentActivity.getSupportFragmentManager().findFragmentByTag(tag);
    }

    public Fragment getFragment(String tag) {

        return mFragmentActivity.getSupportFragmentManager().findFragmentByTag(tag);
    }

    public  void addFragment(
            @NonNull Fragment fragment,
            @NonNull String fragmentTag) {

        if (mFragmentActivity == null) {
            return;
        }

        mFragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(mContainerViewId, fragment, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }

    public  void replaceFragment(
            @NonNull Fragment fragment,
            @NonNull String fragmentTag,
            @Nullable String backStackStateName) {

        if (mFragmentActivity == null) {
            return;
        }

        FragmentTransaction transaction = mFragmentActivity
                .getSupportFragmentManager().beginTransaction();

        // R.anim.en
        // transaction
        //         .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

        transaction
                .replace(mContainerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    public  void replaceFragment(
            @NonNull DialogFragment fragment,
            @NonNull String fragmentTag,
            @Nullable String backStackStateName) {

        if (mFragmentActivity == null) {
            return;
        }

        mFragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(mContainerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    public void replaceFragment(RawBaseFragment baseFragment) {

        replaceFragment(
                baseFragment,
                baseFragment.getFragmentTag(),
                baseFragment.getFragmentTag());
    }

    public void addFragment(RawBaseFragment baseFragment) {

        addFragment(
                baseFragment,
                baseFragment.getFragmentTag());
    }


    public void clearCurrentFragmentFromStack() {

        this.pop();
    }

    public void pop() {
        mFragmentActivity.getSupportFragmentManager().popBackStack();
    }

    public void clearBackStack() {

        int backStackEntryCount =  mFragmentActivity.getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < backStackEntryCount; i++) {
            mFragmentActivity.getSupportFragmentManager().popBackStack();
        }
    }

}
