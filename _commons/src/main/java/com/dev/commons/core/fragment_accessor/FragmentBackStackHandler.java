package com.dev.commons.core.fragment_accessor;

import com.dev.commons.general.ui.Toaster;
import com.dev.commons.general.util.Logr;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

/**
    Helper class to manage back-click into zero fragments
 */
public class FragmentBackStackHandler {

    public final static long TAP_BACK_TO_EXIT_INTERVAL = 2000;


    private AppCompatActivity mActivity;

    private long mExitBackPressed;
    private boolean mIsDoubleBackPressEnabled = true;

    public FragmentBackStackHandler(AppCompatActivity activity) {
        mActivity = activity;
    }

    public boolean isDoubleBackPressEnabled() {
        return mIsDoubleBackPressEnabled;
    }

    public void setDoubleBackPressEnabled(boolean doubleBackPressEnabled) {
        mIsDoubleBackPressEnabled = doubleBackPressEnabled;
    }

    // TODO: Fixme because activities that don't have fragment will crash this
    public boolean onBackPressed(RawFragmentAccessor fragmentAccessor) {
        // let the fragment listener handle all onBackPress logic if it can

        // means the real onBackPressed() method should not call it's super impl if true
        boolean isHandled = false;

        try {

            List<Fragment> backstackFragments = mActivity.getSupportFragmentManager().getFragments();

            int backStackEntryCount = mActivity.getSupportFragmentManager().getBackStackEntryCount();

            Logr.debug("backStackEntryCount: " + backStackEntryCount
                                + "backstackFragments.size(): " + backstackFragments.size());

            String activeFragmentTag = fragmentAccessor.getActiveFragmentTag();

            if (backStackEntryCount > 1 && activeFragmentTag != null) {

                Fragment activeFragment = fragmentAccessor.getFragment(activeFragmentTag);
            }
            else {

                handleEmptyFragmentStack();
                isHandled = true;
            }
        }
        catch (Exception ex) {

        }

        return isHandled;
    }

    public void handleEmptyFragmentStack() {

        boolean isClickedTwoTimesRequirementFulfilled = (mIsDoubleBackPressEnabled)
                ? mExitBackPressed + TAP_BACK_TO_EXIT_INTERVAL > System.currentTimeMillis()
                : true; // if  mIsDoubleBackPressEnabled is OFF , then this is always true

        if (isClickedTwoTimesRequirementFulfilled) {

            mActivity.finish();
        }
        else {

            Toaster.debugShort(mActivity, "Tap back button in order to exit");

            mExitBackPressed = System.currentTimeMillis();
        }
    }

}
