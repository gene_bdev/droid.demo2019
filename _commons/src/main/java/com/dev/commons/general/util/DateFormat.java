package com.dev.commons.general.util;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormat {

    @SuppressLint("SimpleDateFormat")
    public static String getFormattedTime(long time) {
        return new SimpleDateFormat("hh:mm a").format(new Date(time));
    }

    @SuppressLint("SimpleDateFormat")
    public static String getFormattedTimeForChat(long time) {
        return new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").format(new Date(time));
    }

}
