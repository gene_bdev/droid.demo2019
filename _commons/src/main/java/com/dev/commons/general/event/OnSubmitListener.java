package com.dev.commons.general.event;

/**
 * Used for things like key press event listener
 */
public interface OnSubmitListener {

    void onSubmit();
}
