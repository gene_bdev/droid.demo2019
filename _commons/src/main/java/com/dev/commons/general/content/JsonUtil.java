package com.dev.commons.general.content;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.InputStream;

public class JsonUtil {

    public static Gson sGson;

    private static void verifyInit() {

        if (sGson == null) {

            sGson  = new Gson();
        }
    }

    public static String readJSONFromAsset(Context context, String jsonFilename) {

        verifyInit();

        String json = null;
        try {
            InputStream is = context.getAssets().open(jsonFilename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static JsonArray getAssetTextAsJsonArray(Context context, String jsonAssetPath) {

        try {

            String jsonStr = JsonUtil.readJSONFromAsset(context, jsonAssetPath);

            JsonElement jsonTopLevelEl = sGson.fromJson(jsonStr, JsonElement.class);

            JsonArray jsonAssetAsJsonArray = jsonTopLevelEl.getAsJsonArray();

            return jsonAssetAsJsonArray;
        }
        catch (Exception ex) {

            return null;
        }
    }

}
