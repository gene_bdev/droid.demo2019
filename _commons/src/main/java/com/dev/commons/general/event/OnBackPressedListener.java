package com.dev.commons.general.event;

public interface OnBackPressedListener {

    void onBackPressed();
}
