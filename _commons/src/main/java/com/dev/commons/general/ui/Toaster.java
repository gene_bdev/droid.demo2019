package com.dev.commons.general.ui;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * A wrapper around the Android Toast
 * 
 * @author Blundell
 * 
 */
public class Toaster {

    public static void debugShort(Context context, String msg) {
        Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public static void log(String msg) {
        Log.d("debug", msg);
    }

    public static void shortDebug(Context context, String msg) {

        Toast helloToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        // helloToast.setGravity(Gravity.CENTER, 0, 0);
        helloToast.show();
    }

    public static void debugShort(Activity activity, String msg) {
        Toast.makeText(activity.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private final Context context;

    public Toaster(Context context) {
        this.context = context;
    }

    public void popBurntToast(String msg) {
        makeToast(msg, Toast.LENGTH_LONG).show();
    }

    public void popToast(String msg) {
        makeToast(msg, Toast.LENGTH_SHORT).show();
    }

    private Toast makeToast(String msg, int length) {
        return Toast.makeText(context, msg, length);
    }

    public static void unexpectedError(Context context, String msg) {
        msg = "Unexpected: " + msg;
        Toaster.longDebug(context, msg);
    }

    private static void longDebug(Context context, String msg) {

        Toast helloToast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        // helloToast.setGravity(Gravity.CENTER, 0, 0);
        helloToast.show();
    }


}
