This project aims to demonstrate different aspects of Android best practices.

Below is a listing of the main features:



=====================
=====================
Architecture/Design Related Considerations:

1. Using /_commons lib module to distribute unnecessary complexity, out of the mainline /app module

2. Top level Java package structure is clean and well compartmentalized
    * *Main view packages (activity, fragment, and model)*  are at the top level
    * Then for all the other possible things we can add in, there is /platform to wrap that business-logic



=====================
=====================
Android libs/APIs:

1. Retrofit
    * Making 1 @GET call to public JSON at https://jsonplaceholder.typicode.com/todos

2. RxJava with Retrofit
    * Hooked in a basic RxJava setup with the existing retrofit configuation

3. Dagger2
    * Several Java objects can now be added via `@Inject`

4. Gson
    * Mapping/"marshalling" json strings into POJO java objects without a bunch of `get/set` calls



=====================
=====================
Handy/Powerful Utils:

1. `package com.dev.commons.core.fragment_accessor;`
    * Provides a clean solution to:
    
        * Wrap common multi-line calls to FragmentManager (eg, .replaceFragment())
    
        * Manage double back-click to exit a fragment container activity



=====================
=====================
Whats Next:

- Refactor Java to well structured Kotlin: eta 2-3 days
