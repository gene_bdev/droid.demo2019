package com.dev.demo2019.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dev.commons.general.event.OnObjectReadyListener;
import com.dev.demo2019.R;
import com.dev.demo2019.databinding.ActivityStartBinding;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.model.repository.AppState;
import com.dev.demo2019.model.repository.AppStateDao;
import com.dev.demo2019.platform.app.TheApp;
import com.dev.demo2019.platform.base.AppBaseActivity;
import com.dev.demo2019.platform.retrofit.client.AppRetrofitHttpCaller;

import java.util.List;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;

public class StartActivity extends AppBaseActivity {

    private ActivityStartBinding mBinding;

    @Inject
    AppStateDao mAppStateDiDao;

    public StartActivity() {

        TheApp.graph().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start);

        mBinding.btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(StartActivity.this, FragmentContainerActivity.class);
                startActivity(intent);

                finish();
            }
        });

        AppRetrofitHttpCaller
                .getTodoItems(
                        StartActivity.this,
                        new OnObjectReadyListener() {

                            @Override
                            public void onReady(Object object) {

                                List<TodoItem> todoItems = (List<TodoItem>) object;

                                AppState appState = mAppStateDiDao.getAppState();

                                appState.setTodoItems(todoItems);

                                mBinding.progressBar.setVisibility(View.GONE);
                                mBinding.btnGo.setVisibility(View.VISIBLE);
                            }
                        });
    }
}
