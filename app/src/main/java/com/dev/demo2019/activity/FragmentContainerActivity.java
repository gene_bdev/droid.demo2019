package com.dev.demo2019.activity;

import android.os.Bundle;

import com.dev.commons.core.fragment_accessor.FragmentBackStackHandler;
import com.dev.commons.core.fragment_accessor.RawFragmentAccessor;
import com.dev.demo2019.R;
import com.dev.demo2019.databinding.ActivityFragmentContainerBinding;
import com.dev.demo2019.fragment.ItemsListFragment;
import com.dev.demo2019.platform.app.TheApp;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class FragmentContainerActivity extends AppCompatActivity {

    // not actually used, but keeping reference in case
    private ActivityFragmentContainerBinding mFragmentContainerBinding;

    private FragmentBackStackHandler mFragmentBackStackHandler;

    @Inject
    RawFragmentAccessor mFragmentAccessor;

    public FragmentContainerActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFragmentBackStackHandler =
                new FragmentBackStackHandler(FragmentContainerActivity.this);

        mFragmentBackStackHandler.setDoubleBackPressEnabled(true);

        setContainer_forFragmentAccessor(
                R.layout.activity_fragment_container,
                R.id.main_app_fragment_container
        );

        // requires that above line happen 1st in the code before an @inject call is made
        TheApp.graph().inject(this);

        mFragmentAccessor
                .addFragment(ItemsListFragment.newInstance());
    }

    protected void setContainer_forFragmentAccessor(int layoutResId, int fragmentContainerResId) {

        // not actually used, but keeping reference in case
        mFragmentContainerBinding = DataBindingUtil.setContentView(this, layoutResId);

        RawFragmentAccessor.setFragmentContainer(this, fragmentContainerResId);
    }


    @Override
    public void onBackPressed() {

        // let the fragment listener handle all onBackPress logic if it can
        boolean isHandled = mFragmentBackStackHandler
                .onBackPressed(mFragmentAccessor);

        if (! isHandled) {

            super.onBackPressed();  // this exits the app.
        }
    }

}
