package com.dev.demo2019.platform.dagger_di;

import android.content.Context;

import com.dev.commons.core.fragment_accessor.FragmentBackStackHandler;
import com.dev.demo2019.activity.FragmentContainerActivity;
import com.dev.demo2019.activity.StartActivity;
import com.dev.demo2019.fragment.ItemDetailsFragment;
import com.dev.demo2019.fragment.ItemsListFragment;
import com.dev.demo2019.model.adapter.TodoItemListAdapter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        // Graph consists of two modules.
        // Modules are just for convenience, we can put all dependencies in one module.
        // This graph will resolve all dependencies specified in those 2 modules
        modules = {
                AppModule.class,
                StorageModule.class
        })
public interface AppGraph {

    void inject(StartActivity startActivity);

    void inject(FragmentContainerActivity startActivity);

    void inject(FragmentBackStackHandler fragmentBackStackHandler);

    void inject(TodoItemListAdapter todoItemListAdapter);

    void inject(ItemsListFragment itemsListFragment);

    void inject(ItemDetailsFragment itemDetailsFragment);

    Context getContext();
}

