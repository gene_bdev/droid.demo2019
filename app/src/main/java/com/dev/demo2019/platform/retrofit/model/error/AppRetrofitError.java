package com.dev.demo2019.platform.retrofit.model.error;


import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by vidyanandmishra on 13/12/16.
 */
public class AppRetrofitError {

    /**
     * Common method to handle all onFailure call back
     *
     * @param context
     * @param errObj
     */
    public static void handleError(Context context, Object errObj) {

        if (errObj instanceof ErrorResponse) {
            ErrorResponse errModel = (ErrorResponse) errObj;
            if (errModel != null) {
                errHandleDialog(context, errModel);
            }
        }
        else if (errObj instanceof String) {
            String errMsg = (String) errObj;
            errHandleDialog(context, errMsg);
        }
        else {
            Throwable throwable = (Throwable) errObj;
            if (throwable instanceof IOException) {
                Toast.makeText(context, "No internet connection available!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void errHandleDialog(final Context context, String errorMsg) {

        final AlertDialog alert = new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(errorMsg)
                .setPositiveButton("Ok", null).create();

        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }


    /**
     * Showing error dialog
     *
     * @param context
     * @param errorResponse
     */
    public static void errHandleDialog(final Context context, final ErrorResponse errorResponse) {

        final AlertDialog alert = new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(errorResponse.getError().getReason())
                .setPositiveButton("Ok", null).create();

        alert.setCanceledOnTouchOutside(false);
        alert.show();

        Button btnOkay = alert.getButton(AlertDialog.BUTTON_POSITIVE);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (errorResponse.getError().getCode()) {

                    case 1001:
                        //TODO: DO YOUR THINGS AS PER THE ERROR CODE
                        break;

                    case 1002:
                        //TODO: DO YOUR THINGS AS PER THE ERROR CODE
                        break;

                    case 1003:
                        //TODO: DO YOUR THINGS AS PER THE ERROR CODE
                        break;

                    // SO ON ...
                }

                alert.dismiss();
            }
        });
    }
}
