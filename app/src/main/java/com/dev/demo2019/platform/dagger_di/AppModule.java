package com.dev.demo2019.platform.dagger_di;

import android.app.Application;
import android.content.Context;

import com.dev.commons.core.fragment_accessor.RawFragmentAccessor;
import com.dev.demo2019.model.mapper.AppMapper;
import com.dev.demo2019.model.repository.AppStateDao;
import com.dev.demo2019.model.repository.SharedPrefsDao;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext(){
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson(){
        return new Gson();
    }

    @Provides
    @Singleton
    AppStateDao provideAppStateDao(SharedPrefsDao sharedPrefsDao, Gson gson) {
        // SharedPrefsDao is provided by StorageModule (provideSharedPrefsDao method)
        // Gson is provided by provideGson method in this module
        return new AppStateDao(sharedPrefsDao, gson);
    }

    @Provides
    @Singleton
    AppMapper provideAppMapper() {
        return new AppMapper();
    }

    @Provides
    @Singleton
    RawFragmentAccessor provideRawFragmentAccessor() {
        return RawFragmentAccessor.getInstance();
    }

}
