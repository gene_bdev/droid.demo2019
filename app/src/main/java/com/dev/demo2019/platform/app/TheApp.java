package com.dev.demo2019.platform.app;

import android.app.Application;
import android.content.Context;

import com.dev.commons.general.ui.Toaster;
import com.dev.demo2019.platform.dagger_di.AppGraph;
import com.dev.demo2019.platform.dagger_di.AppModule;
import com.dev.demo2019.platform.dagger_di.DaggerAppGraph;


/**
 * Provides context independent of current Activity
 */
public class TheApp extends Application {

    private static AppGraph sAppGraph;

    private static Context sContext;

    public static Context getAppContext() {
        return TheApp.sContext;
    }

    public static void toastShort(String msg) {

        Toaster.debugShort(TheApp.sContext, msg);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        TheApp.sContext = getApplicationContext();

        // Now all dependencies are constructed and live in this graph.
        sAppGraph = DaggerAppGraph
                .builder()
                .appModule(new AppModule(this))
                .build();

    }

    public static AppGraph graph() {
        return sAppGraph;
    }
}
