package com.dev.demo2019.platform.retrofit.end_points;


import com.dev.demo2019.model.pojo.TodoItem;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;

public interface AppRetrofitEndpoints {

    @GET("/todos")
    Call<List<TodoItem>> getTodoItems();

    @GET("/todos")
    Single<Response<List<TodoItem>>> getTodoItemsRx();
}
