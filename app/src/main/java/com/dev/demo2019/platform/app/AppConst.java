package com.dev.demo2019.platform.app;

import android.app.AlertDialog;

public class AppConst {

    public static class Field {

        public final static String id = "id";
        public final static String title = "title";
        public final static String completed = "completed";
    }

    public class Extra {

        public final static String LIST_INDEX = "list_index";
    }

    public static class Dialog {

        public final static int CENTER_BUTTON = AlertDialog.BUTTON_NEUTRAL;
        public final static int LEFT_BUTTON = AlertDialog.BUTTON_NEGATIVE;
        public final static int RIGHT_BUTTON = AlertDialog.BUTTON_POSITIVE;
    }

}


    // public static class Default {
    //
    //     public final static int INVALID_INDEX = -1;
    // }
    //
    // public class Size {
    //
    //     public final static int THUMBNAIL_SCALE = 200;
    //     public final static int SMALL_MEDIUM = 400;
    //
    //     public static final int DEFAULT_MSG_LENGTH_LIMIT = 200;
    // }
