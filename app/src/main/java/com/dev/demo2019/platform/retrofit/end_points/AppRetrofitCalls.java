package com.dev.demo2019.platform.retrofit.end_points;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.dev.commons.general.event.OnObjectReadyListener;
import com.dev.commons.general.util.Logr;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.platform.retrofit.model.error.AppRetrofitError;
import com.dev.demo2019.platform.retrofit.model.error.ErrorResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppRetrofitCalls {

    private static final String TAG = "Presenter";

    private Activity mActivity; // Only used for error handling, to show dialog
    private AppRetrofitNetworkService mNetworkService;

    private OnObjectReadyListener mOnErrorListener;

    public AppRetrofitCalls(Activity activity, String baseUrl) {
        mActivity = activity;
        mNetworkService = new AppRetrofitNetworkService(baseUrl);
    }

    /**
     * Caller can set a custom listener for errors, otherwise handled in standard encapsulated manner
     *
     * @param onErrorListener
     */
    public void setOnErrorListener(OnObjectReadyListener onErrorListener) {
        mOnErrorListener = onErrorListener;
    }

    /**
     * Method to call Login API
     *
     * @param listener
     */
    public void getTodoItems(final OnObjectReadyListener listener) {

        AppRetrofitEndpoints appRetrofitEndpoints = getAppRetrofitEndpoints();
        Call<List<TodoItem>> call = appRetrofitEndpoints.getTodoItems();

        call.enqueue(new Callback<List<TodoItem>>() {

            @Override
            public void onResponse(Call<List<TodoItem>> call, Response<List<TodoItem>> response) {

                List<TodoItem> payloadResponse = processValidPayload(response);
                if (payloadResponse == null)
                    return;

                listener.onReady(payloadResponse);
            }

            @Override
            public void onFailure(Call<List<TodoItem>> call, Throwable errorThrowable) {
                handleError(errorThrowable);
            }
        });
    }

    @SuppressLint("CheckResult")
    public void getTodoItemsRx(final OnObjectReadyListener listener) {
        AppRetrofitEndpoints appRetrofitEndpoints = getAppRetrofitEndpoints();
        appRetrofitEndpoints
                .getTodoItemsRx()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Consumer<Response<List<TodoItem>>>() {

                            @Override
                            public void accept(Response<List<TodoItem>> response) {
                                List<TodoItem> payloadResponse = processValidPayload(response);
                                if (payloadResponse == null)
                                    return;
                                listener.onReady(payloadResponse);
                            }
                        },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                                handleError(throwable);
                            }
                        }
                );
    }

    private AppRetrofitEndpoints getAppRetrofitEndpoints() {

        AppRetrofitEndpoints appRetrofitEndpoints =
                (AppRetrofitEndpoints) mNetworkService.getAPI(AppRetrofitEndpoints.class);

        return appRetrofitEndpoints;
    }

    // ==========================
    // Error Handling

    /**
     * @param response
     * @return true if error occurred and was handled
     */
    private List<TodoItem> processValidPayload(Response<List<TodoItem>> response) {

        List<TodoItem> payloadResponse = null;

        if (response.isSuccessful() != true) {

            // ResponseBody errorBody = response.errorBody();
            handleError(response);
        }
        else {

            payloadResponse = response.body();
        }


        return payloadResponse;
    }

    private void handleError(Response response) {

        ResponseBody errorBody = response.errorBody();

        ErrorResponse errorModel = null;

        try {

            String errorBodyStr = errorBody.string();
            JSONObject jObjError = new JSONObject(errorBodyStr);
            errorModel = new Gson().fromJson(jObjError.toString(), ErrorResponse.class);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        handleError(errorModel);
    }

    private void handleError(Throwable errorThrowable) {

        Logr.debug("Throwable: " + errorThrowable.getMessage());

        errorThrowable.printStackTrace();
        handleErrorCommon(errorThrowable);
    }

    private void handleError(String errMsg) {

        Logr.debug("handleError errMsg: " + errMsg);
        handleErrorCommon(errMsg);
    }

    private void handleError(ErrorResponse errorResponse) {

        handleErrorCommon(errorResponse);
    }

    private void handleErrorCommon(Object error) {

        AppRetrofitError.handleError(mActivity, error);

        if (mOnErrorListener != null) {

            mOnErrorListener.onReady(error);
        }
    }
}

