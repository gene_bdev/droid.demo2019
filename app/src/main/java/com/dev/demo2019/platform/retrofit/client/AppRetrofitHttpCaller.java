package com.dev.demo2019.platform.retrofit.client;

import android.app.Activity;

import com.dev.commons.general.event.OnObjectReadyListener;
import com.dev.commons.general.network.HttpUtil;
import com.dev.commons.general.util.Logr;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.platform.retrofit.end_points.AppRetrofitCalls;

import java.util.List;

/**
 * So far, this is the only class that should exposed
 * If we dont accomplish this w/ProGuard, then in-elegant workaround would be to just put all .class files in same package, set them to protected except Main public one
 */
public class AppRetrofitHttpCaller {

    private final static String BASE_URL = "https://jsonplaceholder.typicode.com";

    public static void getTodoItems(Activity activity, final OnObjectReadyListener onNonceReadyListener) {

        boolean hasNetworkAccess = HttpUtil.isNetworkAvailable(activity.getApplicationContext());

        if (hasNetworkAccess != true) {

            // Toaster.debugShort(mActivity, "Wifi not found, DevApp  Unavailable ");
            Logr.debug("Wifi not found, DevApp Unavailable ");

            return;
        }

        AppRetrofitCalls appRetrofitCalls = new AppRetrofitCalls(activity, BASE_URL);

        appRetrofitCalls
                .getTodoItemsRx(new OnObjectReadyListener() {

                    @Override
                    public void onReady(Object response) {

                        List<TodoItem> todoItems = (List<TodoItem> ) response;
                        onNonceReadyListener.onReady(todoItems);
                    }
                });

    }

}




