package com.dev.demo2019.platform.retrofit.end_points;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRetrofitNetworkService {

    private static final String TAG = "RetrofitNetworkService";

    private OkHttpClient mOkHttpClient;
    private Retrofit mRetrofit;

    protected AppRetrofitNetworkService(String baseUrl) {

        mOkHttpClient = buildClient();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(mOkHttpClient)
                .build();
    }

    /**
     * Getting API Interface
     * @return api interface
     */
    protected Object getAPI(Class retrofitApi_interface) {
        // javaDoc: Create an implementation of the API endpoints defined by the {@code service} interface.
        return mRetrofit.create(retrofitApi_interface);
    }


    /**
     * Building and returning an OkHttpClient and
     * adding custom headers in efficient manner
     * @return okhttpclient
     */
    protected OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        // Bellow five lines is only for debugging purpose
        // If you want to print url, header etc on console
        // If required you can keep of remove
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        builder.addInterceptor(logging);

        builder.addInterceptor(new Interceptor() {

            @Override
            public Response intercept(Chain chain) throws IOException {

                Request request;

                request = chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .build();

                // AppPreferenceManager appPref = AppPreferenceManager.getInstance();
                // Here, can add multiple headers as per your need.
                // If user session is valid it will execute if{} otherwise else{}
                // if (appPref.hasSession()) {
                //     request = chain.request().newBuilder()
                //             .addHeader("Accept", "application/json")
                //             .addHeader(X_API_KEY, X_API_VALUE)
                //             .addHeader(X_SESSION_KEY, appPref.getSessionKey()).build();
                // } else {
                //     request = chain.request().newBuilder()
                //             .addHeader("Accept", "application/json")
                //             .addHeader(X_API_KEY, X_API_VALUE).build();
                // }
                //
                // // This if{} is only to print the request body if you want to.
                // if (request.body() != null) {
                //     Buffer buffer = new Buffer();
                //     request.body().writeTo(buffer);
                //     String body = buffer.readUtf8();
                //     Log.i(TAG, "intercept: Request Body: " + body);
                // }

                return chain.proceed(request);
            }
        });

        return builder.build();
    }
}
