package com.dev.demo2019.platform.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

abstract public class AppBaseActivity extends AppCompatActivity {

    // protected AppState mAppState;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // mAppState = AppStateDao.getAppState();
    }
}
