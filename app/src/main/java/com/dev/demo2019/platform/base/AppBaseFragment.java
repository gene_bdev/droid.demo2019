package com.dev.demo2019.platform.base;


import android.os.Bundle;
import android.view.View;

import com.dev.commons.core.fragment_accessor.app_fragment.RawBaseFragment;

import androidx.annotation.Nullable;

public class AppBaseFragment extends RawBaseFragment {

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}

