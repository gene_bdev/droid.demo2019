package com.dev.demo2019.platform.dagger_di;

import android.content.Context;
import android.content.SharedPreferences;

import com.dev.demo2019.model.repository.SharedPref;
import com.dev.demo2019.model.repository.SharedPrefsDao;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class StorageModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context){
        // Context is provided by AppModule (provideContext method)
        return context.getSharedPreferences(SharedPref.PREF__APP_STATE, 0);
    }

    @Provides
    @Singleton
    SharedPrefsDao provideSharedPrefsDao(SharedPreferences sharedPreferences, Gson gson) {
        // SharedPreferences is provided by provideSharedPreferences method of this module
        // Gson is provided by AppModule (provideGson method)
        return new SharedPrefsDao(sharedPreferences, gson);
    }

}
