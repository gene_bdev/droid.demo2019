package com.dev.demo2019.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.commons.general.ui.DialogUtil;
import com.dev.demo2019.R;
import com.dev.demo2019.databinding.FragmentItemDetailsBinding;
import com.dev.demo2019.model.mapper.AppMapper;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.model.repository.AppStateDao;
import com.dev.demo2019.model.view.TodoItemViewModel;
import com.dev.demo2019.platform.app.AppConst;
import com.dev.demo2019.platform.app.TheApp;
import com.dev.demo2019.platform.base.AppBaseFragment;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class ItemDetailsFragment extends AppBaseFragment {

    public final static String TAG = "ItemDetailsFragment";

    private FragmentItemDetailsBinding mBinding;

    @Inject
    AppStateDao mAppStateDiDao;

    @Inject
    AppMapper mAppMapper;

    private Integer mItemPosition;
    private TodoItem mTodoItem;

    public ItemDetailsFragment() {

        TheApp.graph().inject(this);
    }

    public static ItemDetailsFragment newInstance(int itemPosition) {

        ItemDetailsFragment fragment = new ItemDetailsFragment();

        Bundle args = new Bundle();
        args.putInt(AppConst.Extra.LIST_INDEX, itemPosition);
        fragment.setArguments(args);

        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil
                .inflate(inflater,
                        R.layout.fragment_item_details,
                        container,
                        false);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadViewContent();
    }

    private void loadViewContent() {

        boolean isViewValid = false;

        Bundle bundle = getArguments();
        if (bundle != null) {

            mItemPosition = bundle.getInt(AppConst.Extra.LIST_INDEX);

            if (mItemPosition != null) {

                mTodoItem = mAppStateDiDao
                        .getAppState()
                        .getTodoItem(mItemPosition);

                TodoItemViewModel viewModel = mAppMapper.getViewModel(mTodoItem);

                mBinding.setTodoItem(viewModel);
            }

            if (mTodoItem != null) {

                isViewValid = true;
            }

        }

        if (isViewValid != true) {

            DialogUtil
                    .getErrorDialog(
                            getActivity(),
                            getString(R.string.unexpected_error),
                            getString(R.string.error_missing_expected_data)
                    )
                    .show();

            return;
        }
    }
}
