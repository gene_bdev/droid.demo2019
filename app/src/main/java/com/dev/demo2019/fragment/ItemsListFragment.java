package com.dev.demo2019.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.commons.core.fragment_accessor.RawFragmentAccessor;
import com.dev.commons.general.event.ItemClickListener;
import com.dev.commons.general.util.Logr;
import com.dev.demo2019.R;
import com.dev.demo2019.databinding.FragmentItemsListBinding;
import com.dev.demo2019.model.adapter.TodoItemListAdapter;
import com.dev.demo2019.model.mapper.AppMapper;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.model.repository.AppStateDao;
import com.dev.demo2019.platform.app.TheApp;
import com.dev.demo2019.platform.base.AppBaseFragment;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class ItemsListFragment extends AppBaseFragment {

    public final static String TAG = "ItemsListFragment";

    private FragmentItemsListBinding mBinding;

    @Inject
    Context mContext;

    @Inject
    AppStateDao mAppStateDiDao;

    @Inject
    AppMapper mAppMapper;

    private TodoItemListAdapter mTodoItemListAdapter;

    public ItemsListFragment() {

        TheApp.graph().inject(this);
    }

    public static ItemsListFragment newInstance() {
        return new ItemsListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil
                .inflate(inflater,
                        R.layout.fragment_items_list,
                        container,
                        false);

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setItemsList();
    }

    /**
     * Option to test with local .json file under /assets:
     *  List<TodoItem> todoItems = mAppMapper.getAssetTodoItems();
     */
    private void setItemsList() {

        List<TodoItem> todoItems = mAppStateDiDao
                .getAppState()
                .getTodoItems();

        mTodoItemListAdapter =
                new TodoItemListAdapter(todoItems);

        mTodoItemListAdapter
                .setClickListener(new ItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String msg = "position: " + position;
                        // Toaster.debugShort(getContext(), msg);
                        Logr.debug(msg);

                        RawFragmentAccessor
                                .getInstance()
                                .replaceFragment(
                                        ItemDetailsFragment.newInstance(position)
                                );
                    }
                });

        mBinding.itemList.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.itemList.setAdapter(mTodoItemListAdapter);

        Logr.debug("todosArrayJsonObject: " + todoItems);
    }

}


