package com.dev.demo2019.model.view;

import com.dev.demo2019.platform.app.AppConst;
import com.google.gson.annotations.SerializedName;

public class TodoItemViewModel {

    @SerializedName(AppConst.Field.title)
    private String mTitle;

    @SerializedName(AppConst.Field.completed)
    private String mCompletedStatus;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getCompletedStatus() {
        return mCompletedStatus;
    }

    public void setCompletedStatus(String completedStatus) {
        mCompletedStatus = completedStatus;
    }
}
