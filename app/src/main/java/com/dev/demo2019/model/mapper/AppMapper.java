package com.dev.demo2019.model.mapper;

import android.content.Context;

import com.dev.commons.general.content.JsonUtil;
import com.dev.demo2019.R;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.model.view.TodoItemViewModel;
import com.dev.demo2019.platform.app.TheApp;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class AppMapper {

    private Context mContext;
    private Gson mGson;

    public AppMapper() {

        mContext = TheApp.getAppContext();

        mGson = new Gson();
    }

    public List<TodoItem> getAssetTodoItems() {

        String todosJsonAssetPath = "json/typicodeTodos.json";

        JsonArray jsonAssetAsJsonArray = JsonUtil
                .getAssetTextAsJsonArray(mContext, todosJsonAssetPath);

        List<TodoItem> todoItems =
                mGson
                        .fromJson(
                                jsonAssetAsJsonArray,
                                new TypeToken<List<TodoItem>>() {
                                }.getType()
                        );

        return todoItems;
    }


    public TodoItemViewModel getViewModel(TodoItem todoItem) {

        TodoItemViewModel viewModel = new TodoItemViewModel();

        int statusStringResId = (todoItem.isCompleted())
                ? R.string.completed_status_true
                : R.string.completed_status_false;

        String completedStatus = mContext.getString(statusStringResId);

        viewModel.setTitle(todoItem.getTitle());
        viewModel.setCompletedStatus(completedStatus);

        return viewModel;
    }
}
