package com.dev.demo2019.model.pojo;

import com.dev.demo2019.platform.app.AppConst;
import com.google.gson.annotations.SerializedName;

public class TodoItem {

    @SerializedName(AppConst.Field.id)
    private String mId;

    @SerializedName(AppConst.Field.title)
    private String mTitle;

    @SerializedName(AppConst.Field.completed)
    private boolean mIsCompleted;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean isCompleted() {
        return mIsCompleted;
    }

    public void setCompleted(boolean completed) {
        mIsCompleted = completed;
    }
}
