package com.dev.demo2019.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.commons.general.event.ItemClickListener;
import com.dev.commons.general.util.Logr;
import com.dev.demo2019.R;
import com.dev.demo2019.databinding.RowTodoItemBinding;
import com.dev.demo2019.model.mapper.AppMapper;
import com.dev.demo2019.model.pojo.TodoItem;
import com.dev.demo2019.model.view.TodoItemViewModel;
import com.dev.demo2019.platform.app.TheApp;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TodoItemListAdapter extends RecyclerView.Adapter<TodoItemListAdapter.TodoItemViewHolder> {

    @Inject
    Context mContext;

    @Inject
    AppMapper mAppMapper;

    private List<TodoItem> mTodoItems = Collections.emptyList();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public TodoItemListAdapter(List<TodoItem> todoItems) {

        TheApp.graph().inject(this);

        mInflater = LayoutInflater.from(mContext);
        mTodoItems = todoItems;
    }

    public void setTodoItems(List<TodoItem> todoItems) {
        mTodoItems = todoItems;
    }

    @Override
    public TodoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RowTodoItemBinding binding =
                DataBindingUtil.inflate(
                        mInflater,
                        R.layout.row_todo_item,
                        parent,
                        false);

        return new TodoItemViewHolder(binding);
    }



    @Override
    public void onBindViewHolder(TodoItemViewHolder holder, final int position) {

        TodoItem todoItem = mTodoItems.get(position);

        TodoItemViewModel todoItemViewModel = mAppMapper.getViewModel(todoItem);

        Logr.debug("list adapter loads todoItemViewModel: " + todoItemViewModel);

        holder.getBinding().setTodoItem(todoItemViewModel);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        mClickListener = itemClickListener;
    }


    @Override
    public int getItemCount() {
        return mTodoItems.size();
    }

    // convenience method for getting data at click position
    public TodoItem getItem(int id) {
        return mTodoItems.get(id);
    }

    public class TodoItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RowTodoItemBinding mBinding;

        public TodoItemViewHolder(RowTodoItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mBinding.getRoot().setOnClickListener(this);
            mBinding.executePendingBindings();
        }

        public RowTodoItemBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View view) {

            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }

}
