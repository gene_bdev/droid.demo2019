package com.dev.demo2019.model.repository;

import android.content.SharedPreferences;

import com.google.gson.Gson;

public class SharedPrefsDao {

    private final SharedPreferences mSharedPreferences;
    private final Gson mGson;

    public SharedPrefsDao(SharedPreferences sharedPreferences, Gson gson) {
        mSharedPreferences = sharedPreferences;
        mGson = gson;
    }

    void writeSerializableToJson(Object serializableObj, String prefKey) {
        String sharedPrefJsonStr = mGson.toJson(serializableObj);
        updatePrefs(prefKey, sharedPrefJsonStr);
    }

    String getSharedPrefJsonString(String prefKey) {
        String encodedJsonStr = getPrefString(prefKey);
        if (encodedJsonStr.isEmpty()) {
            return "";
        }
        return encodedJsonStr;
    }

    private String getPrefString(String prefName) {
        return mSharedPreferences.getString(prefName, "");
    }

    private void updatePrefs(String prefName, String value) {
        mSharedPreferences
                .edit()
                .putString(prefName, value)
                .apply();
    }

}


