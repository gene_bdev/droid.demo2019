package com.dev.demo2019.model.repository;

import com.google.gson.Gson;

public class AppStateDao {

    private final SharedPrefsDao mSharedPrefsDiDao;
    private final Gson mGson;
    private AppState mAppState = new AppState();

    public AppStateDao(SharedPrefsDao sharedPrefsDiDao, Gson gson) {
        mSharedPrefsDiDao = sharedPrefsDiDao;
        mGson = gson;
        loadInitialState();
    }

    private void loadInitialState() {
        String appStateJsonStr = mSharedPrefsDiDao.getSharedPrefJsonString(SharedPref.PREF__APP_STATE);
        if (appStateJsonStr.isEmpty()) {
            return;
        }

        // Load existing data
        try {
            mAppState = mGson.fromJson(appStateJsonStr, AppState.class);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public AppState getAppState() {
        return mAppState;
    }

    public void updateAppState(AppState appState) {
        mAppState = appState;
        mSharedPrefsDiDao.writeSerializableToJson(appState, SharedPref.PREF__APP_STATE);
    }


}



