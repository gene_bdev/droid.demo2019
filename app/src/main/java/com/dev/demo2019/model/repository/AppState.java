package com.dev.demo2019.model.repository;

import com.dev.commons.general.util.Logr;
import com.dev.demo2019.model.pojo.TodoItem;

import java.util.ArrayList;
import java.util.List;

public class AppState {

    private List<TodoItem> mTodoItems = new ArrayList<>();

    public List<TodoItem> getTodoItems() {
        return mTodoItems;
    }

    public TodoItem getTodoItem(int index) {

        TodoItem todoItem = mTodoItems.get(index);

        return todoItem;
    }

    public void setTodoItems(List<TodoItem> todoItems) {

        if (todoItems == null) {

            String devErrMsg = "Usage error - input is null for: setTodoItems(List<TodoItem> todoItems) ";
            Logr.debug(devErrMsg);
            return;
        }

        mTodoItems = todoItems;
    }
}
